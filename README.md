# Cron Parser

## Name
Cron Expression Parser

## Description
A command line application which parses a cron string and expands each field to show the times at which it will run.

## Checkout and Build

Use below commonds (Assuming Linux environment)

```
git clone https://gitlab.com/niranjangande/cron-expression-parser.git
export JAVA_HOME=</path/to/jdk17>
cd cron-expression-parser
./gradlew build
```

## Usage

```
$JAVA_HOME/bin/java -jar ./build/libs/cron-expression-parser-1.0-SNAPSHOT.jar "0-20 * * * * /my/path/to/command --arg1 arg1Value"

Output
------
minute        0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
hour          0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
day of month  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   0 1 2 3 4 5 6
command       /my/path/to/command --arg1 arg1Value
```


```
$JAVA_HOME/bin/java -jar ./build/libs/cron-expression-parser-1.0-SNAPSHOT.jar "*/15 0 1,15 * 1-5 /usr/bin/find"

Output
------
minute        0 15 30 45
hour          0
day of month  1 15
month         1 2 3 4 5 6 7 8 9 10 11 12
day of week   1 2 3 4 5
command       /usr/bin/find
```

