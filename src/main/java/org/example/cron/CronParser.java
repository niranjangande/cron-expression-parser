package org.example.cron;

import org.example.cron.format.CronJobDefFormatter;
import org.example.cron.model.CronJobDefinition;
import org.example.cron.parser.CronExpressionParser;

public class CronParser {
    private final CronExpressionParser expressionParser;
    private final CronJobDefFormatter formatter;

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Please supply exactly one command line input argument");
        }
        CronParser cronParser = new CronParser(new CronExpressionParser(), new CronJobDefFormatter());
        System.out.println(cronParser.process(args[0]));
    }

    CronParser(CronExpressionParser expressionParser, CronJobDefFormatter formatter) {
        this.expressionParser = expressionParser;
        this.formatter = formatter;
    }

    public String process(String cronString) {
        CronJobDefinition cronJobDefinition = expressionParser.parse(cronString);
        return formatter.prettyPrint(cronJobDefinition);
    }
}
