package org.example.cron.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public final class DayOfWeekFieldParser implements CronFieldExpressionParser {
    private static final Logger LOG = Logger.getLogger(DayOfWeekFieldParser.class.getName());
    private static final Pattern DAY_OF_WEEK_PATTERN = Pattern.compile("^(?:(\\?|\\*|\\d+-\\d+)|((\\d+,)*\\d+))$");
    private static final String LOW_TO_HIGH = "*";
    private static final String ANY_DAY = "?";

    private static final int DOW_LOW = 0;
    private static final int DOW_HIGH = 6;

    @Override
    public boolean validateExpression(String expression) {
        Matcher matcher = DAY_OF_WEEK_PATTERN.matcher(expression);
        boolean isValid = matcher.find();
        LOG.log(Level.FINE, "Expr : " + expression);
        if (isValid) {
            IntStream.range(0, matcher.groupCount()).forEach(i -> LOG.log(Level.FINEST, "Group " + i + " :" + matcher.group(i)));
        }
        return isValid;
    }

    @Override
    public List<Integer> parse(String cronFieldExpression) {
        Matcher matcher = DAY_OF_WEEK_PATTERN.matcher(cronFieldExpression);
        boolean isValid = matcher.find();
        LOG.log(Level.FINE, "Expr : " + cronFieldExpression);
        if (isValid) {
            IntStream.range(0, matcher.groupCount()).forEach(i -> LOG.log(Level.FINEST, "Group " + i + " :" + matcher.group(i)));
        } else {
            throw new IllegalArgumentException("Invalid cron expression : " + cronFieldExpression);
        }

        String timeUnitRange = matcher.group(1);
        String timeUnitsCommaSeparated = matcher.group(2);

        List<Integer> timeUnitsDerived;
        boolean allInValidRange;
        if (timeUnitsCommaSeparated != null) {
            String[] timeUnits = timeUnitsCommaSeparated.split(",");
            timeUnitsDerived = Arrays.stream(timeUnits).map(Integer::parseInt).sorted().toList();
            allInValidRange = timeUnitsDerived.stream().allMatch(i -> i >= DOW_LOW && i <= DOW_HIGH);
            if (!allInValidRange) {
                throw new IllegalArgumentException("Values not in range");
            }
        } else {
            int timeUnitStart;
            int timeUnitEnd;

            if (timeUnitRange.equals(LOW_TO_HIGH) || timeUnitRange.equals(ANY_DAY)) {
                timeUnitStart = DOW_LOW;
                timeUnitEnd = DOW_HIGH;
            } else {
                String[] timeRangeCriteria = timeUnitRange.split("-");
                timeUnitStart = Integer.parseInt(timeRangeCriteria[0]);
                timeUnitEnd = Integer.parseInt(timeRangeCriteria[1]);
                allInValidRange = IntStream.of(timeUnitStart, timeUnitEnd).allMatch(i -> i >= DOW_LOW && i <= DOW_HIGH);
                if (!allInValidRange) {
                    throw new IllegalArgumentException("Invalid range");
                }
            }

            timeUnitsDerived = new ArrayList<>();
            for (int timeUnit = timeUnitStart; timeUnit <= timeUnitEnd; timeUnit++) {
                timeUnitsDerived.add(timeUnit);
            }
        }

        return timeUnitsDerived;
    }
}
