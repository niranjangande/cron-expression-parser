package org.example.cron.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public final class NumericFieldRangeParser implements CronFieldExpressionParser {
    private static final Logger LOG = Logger.getLogger(NumericFieldRangeParser.class.getName());
    private static final Pattern NUMERIC_PATTERN = Pattern.compile("^(?:(\\*|\\d+-\\d+)(?:/(\\d+))?|((\\d+,)*\\d+))$");
    private static final String LOW_TO_HIGH = "*";

    private final int low;
    private final int high;

    NumericFieldRangeParser(int low, int high) {
        if (low >= high) {
            throw new IllegalArgumentException("Invalid range specification. Low cannot be greater than high");
        }
        this.low = low;
        this.high = high;
    }

    @Override
    public boolean validateExpression(String expression) {
        Matcher matcher = NUMERIC_PATTERN.matcher(expression);
        boolean isValid = matcher.find();
        LOG.log(Level.FINE, "Expr : " + expression);
        if (isValid) {
            IntStream.range(0, matcher.groupCount()).forEach(i -> LOG.log(Level.FINE, "Group " + i + " :" + matcher.group(i)));
        }
        return isValid;
    }

    @Override
    public List<Integer> parse(String cronFieldExpression) {
        Matcher matcher = NUMERIC_PATTERN.matcher(cronFieldExpression);
        boolean isValid = matcher.find();
        LOG.log(Level.FINE, "Expr : " + cronFieldExpression);
        if (isValid) {
            IntStream.range(0, matcher.groupCount()).forEach(i -> LOG.log(Level.FINER, "Group " + i + " :" + matcher.group(i)));
        } else {
            throw new IllegalArgumentException("Invalid cron expression : " + cronFieldExpression);
        }

        String timeUnitRange = matcher.group(1);
        String timeUnitIncrement = matcher.group(2);
        String timeUnitsCommaSeparated = matcher.group(3);


        List<Integer> timeUnitsDerived;
        boolean allInValidRange;
        if (timeUnitsCommaSeparated != null) {
            String[] timeUnits = timeUnitsCommaSeparated.split(",");
            timeUnitsDerived = Arrays.stream(timeUnits).map(Integer::parseInt).sorted().toList();
            allInValidRange = timeUnitsDerived.stream().allMatch(i -> i >= low && i <= high);
            if (!allInValidRange) {
                throw new IllegalArgumentException("Values not in range");
            }
        } else {
            int timeIncr = 1;
            if (timeUnitIncrement != null) {
                timeIncr = Integer.parseInt(timeUnitIncrement);
            }

            int timeUnitStart;
            int timeUnitEnd;

            if (timeUnitRange.equals(LOW_TO_HIGH)) {
                timeUnitStart = this.low;
                timeUnitEnd = this.high;
            } else {
                String[] timeRangeCriteria = timeUnitRange.split("-");
                timeUnitStart = Integer.parseInt(timeRangeCriteria[0]);
                timeUnitEnd = Integer.parseInt(timeRangeCriteria[1]);
            }

            allInValidRange = IntStream.of(timeUnitStart, timeUnitEnd, timeIncr).allMatch(i -> i >= low && i <= high);
            if (!allInValidRange) {
                throw new IllegalArgumentException("Invalid range");
            }
            timeUnitsDerived = new ArrayList<>();
            for (int timeUnit = timeUnitStart; timeUnit <= timeUnitEnd; timeUnit += timeIncr) {
                timeUnitsDerived.add(timeUnit);
            }
        }
        return timeUnitsDerived;
    }
}
