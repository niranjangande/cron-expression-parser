package org.example.cron.parser;

import java.util.List;

public interface CronFieldExpressionParser {
    boolean validateExpression(String expression);
    List<Integer> parse(String cronFieldExpression);
}
