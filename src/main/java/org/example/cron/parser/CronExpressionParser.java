package org.example.cron.parser;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.example.cron.model.CronJobDefinition;
import org.example.cron.model.CronSchedule;

public final class CronExpressionParser {

    private static final int MINUTE_LOW = 0;
    private static final int MINUTE_HIGH = 59;

    private static final int HOUR_LOW = 0;
    private static final int HOUR_HIGH = 23;

    private static final int DOM_LOW = 1;
    private static final int DOM_HIGH = 31;

    private static final int MONTH_LOW = 1;
    private static final int MONTH_HIGH = 12;


    public CronJobDefinition parse(String cronString) {
        String[] cronStringTokens = cronString.split(" ", 6);
        if (cronStringTokens.length != 6) {
            throw new IllegalArgumentException("Expecting cron schedule to have valid Cron Expression with and a command."
                    + " Expecting 5 time fields and one command. Found only " + cronStringTokens.length + " fields");
        }

        String[] cronExpression = Arrays.copyOfRange(cronStringTokens, 0, 5);
        CronSchedule cronSchedule = parseCronExpression(cronExpression);
        String cmd = cronStringTokens[5];
        return new CronJobDefinition(cronSchedule, cmd);
    }

    private CronSchedule parseCronExpression(String[] cronExpression) {
        String minute = cronExpression[0];
        String hour = cronExpression[1];
        String dom = cronExpression[2];
        String month = cronExpression[3];
        String dow = cronExpression[4];

        validateExpression(minute, CronField.MINUTE);
        validateExpression(hour, CronField.HOUR);
        validateExpression(dom, CronField.DAY_OF_MONTH);
        validateExpression(month, CronField.MONTH);
        validateExpression(dow, CronField.DAY_OF_WEEK);

        return CronSchedule.builder()
                .withMinutes(parseSimple(minute, CronField.MINUTE))
                .withHours(parseSimple(hour, CronField.HOUR))
                .withDaysOfMonth(parseSimple(dom, CronField.DAY_OF_MONTH))
                .withMonths(parseSimple(month, CronField.MONTH))
                .withDaysOfWeek(parseSimple(dow, CronField.DAY_OF_WEEK))
                .build();
    }

    private void validateExpression(String expression, CronField timeField) {
        boolean isValid = timeField.getParser().validateExpression(expression);
        if (!isValid) {
            throw new IllegalArgumentException("Invalid " + timeField.name() + " expression");
        }
    }

    private List<Integer> parseSimple(String expression, CronField timeField) {
        return timeField.getParser().parse(expression);
    }

    private enum CronField {
        MINUTE(new NumericFieldRangeParser(MINUTE_LOW, MINUTE_HIGH)),
        HOUR(new NumericFieldRangeParser(HOUR_LOW, HOUR_HIGH)),
        DAY_OF_MONTH(new NumericFieldRangeParser(DOM_LOW, DOM_HIGH)),
        MONTH(new NumericFieldRangeParser(MONTH_LOW, MONTH_HIGH)),
        DAY_OF_WEEK(new DayOfWeekFieldParser());

        CronFieldExpressionParser cronFieldExpressionParser;

        CronField(CronFieldExpressionParser cronFieldExpressionParser) {
            this.cronFieldExpressionParser = cronFieldExpressionParser;
        }

        public CronFieldExpressionParser getParser() {
            return cronFieldExpressionParser;
        }
    }
}
