package org.example.cron.format;

import java.util.List;

import org.example.cron.model.CronJobDefinition;
import org.example.cron.model.CronSchedule;

public class CronJobDefFormatter {

    private static final String FORMAT = "%-14s";
    private static final char NEW_LINE = '\n';
    public static final char SPACE = ' ';

    public String prettyPrint(CronJobDefinition cronJob) {
        CronSchedule cronSchedule = cronJob.getCronSchedule();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format(FORMAT, "minute"));
        listAppend(stringBuilder, cronSchedule.getMinutes());


        stringBuilder.append(NEW_LINE).append(String.format(FORMAT, "hour"));
        listAppend(stringBuilder, cronSchedule.getHours());

        stringBuilder.append(NEW_LINE).append(String.format(FORMAT, "day of month"));
        listAppend(stringBuilder, cronSchedule.getDaysOfMonth());

        stringBuilder.append(NEW_LINE).append(String.format(FORMAT, "month"));
        listAppend(stringBuilder, cronSchedule.getMonths());

        stringBuilder.append(NEW_LINE).append(String.format(FORMAT, "day of week"));
        listAppend(stringBuilder, cronSchedule.getDaysOfWeek());

        stringBuilder.append(NEW_LINE).append(String.format(FORMAT, "command")).append(cronJob.getCmd());
        return stringBuilder.toString();
    }

    private void listAppend(StringBuilder stringBuilder, List<Integer> intList) {
        for (int i = 0; i < intList.size(); i++) {
            if (i > 0) {
                stringBuilder.append(SPACE);
            }
            stringBuilder.append(intList.get(i).intValue());
        }
    }
}
