package org.example.cron.model;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class CronSchedule {
    private List<Integer> minutes;
    private List<Integer> hours;
    private List<Integer> daysOfMonth;
    private final List<Integer> months;
    private List<Integer> daysOfWeek;

    public CronSchedule(List<Integer> minutes, List<Integer> hours, List<Integer> daysOfMonth, List<Integer> months, List<Integer> daysOfWeek) {
        this.minutes = List.copyOf(minutes);
        this.hours = List.copyOf(hours);
        this.daysOfMonth = List.copyOf(daysOfMonth);
        this.months = List.copyOf(months);
        this.daysOfWeek = List.copyOf(daysOfWeek);
    }

    public static Builder builder() {
        return new Builder();
    }

    public List<Integer> getMinutes() {
        return this.minutes;
    }

    public List<Integer> getHours() {
        return this.hours;
    }

    public List<Integer> getDaysOfMonth() {
        return this.daysOfMonth;
    }

    public List<Integer> getMonths() {
        return months;
    }

    public List<Integer> getDaysOfWeek() {
        return this.daysOfWeek;
    }

    @Override
    public String toString() {
        return "CronSchedule{" +
                "minutes=" + minutes +
                ", hours=" + hours +
                ", daysOfMonth=" + daysOfMonth +
                ", months=" + months +
                ", daysOfWeek=" + daysOfWeek +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CronSchedule that = (CronSchedule) o;
        return Objects.equals(minutes, that.minutes) && Objects.equals(hours, that.hours) && Objects.equals(daysOfMonth, that.daysOfMonth) && Objects.equals(months, that.months) && Objects.equals(daysOfWeek, that.daysOfWeek);
    }

    @Override
    public int hashCode() {
        return Objects.hash(minutes, hours, daysOfMonth, months, daysOfWeek);
    }

    public static final class Builder {
        private List<Integer> minutes = Collections.emptyList();
        private List<Integer> hours = Collections.emptyList();
        private List<Integer> daysOfMonth = Collections.emptyList();
        private List<Integer> months = Collections.emptyList();
        private List<Integer> daysOfWeek = Collections.emptyList();

        private Builder(){
        }

        public Builder withMinutes(List<Integer> minutes) {
            this.minutes = List.copyOf(minutes);
            return this;
        }

        public Builder withHours(List<Integer> hours) {
            this.hours = List.copyOf(hours);
            return this;
        }

        public Builder withDaysOfMonth(List<Integer> daysOfMonth) {
            this.daysOfMonth = List.copyOf(daysOfMonth);
            return this;
        }

        public Builder withMonths(List<Integer> months) {
            this.months = List.copyOf(months);
            return this;
        }

        public Builder withDaysOfWeek(List<Integer> daysOfWeek) {
            this.daysOfWeek = List.copyOf(daysOfWeek);
            return this;
        }

        public CronSchedule build() {
            validateNotEmpty();
            CronSchedule cronSchedule = new CronSchedule(minutes, hours, daysOfMonth, months, daysOfWeek);
            reset();
            return cronSchedule;
        }

        private void validateNotEmpty() {
            if (minutes.isEmpty() || hours.isEmpty() || daysOfMonth.isEmpty() || months.isEmpty() || daysOfWeek.isEmpty()) {
                throw new IllegalArgumentException("Some of Cron Schedule fields are empty");
            }
        }

        private void reset() {
            this.minutes = Collections.emptyList();
            this.hours = Collections.emptyList();
            this.daysOfMonth = Collections.emptyList();
            this.months = Collections.emptyList();
            this.daysOfWeek = Collections.emptyList();
        }
    }
}
