package org.example.cron.model;

import java.util.Objects;

public final class CronJobDefinition {
    private CronSchedule cronSchedule;
    private String cmd;

    public CronJobDefinition(CronSchedule cronSchedule, String cmd){
        this.cronSchedule = cronSchedule;
        this.cmd = cmd;
    }

    public CronSchedule getCronSchedule(){
        return this.cronSchedule;
    }

    public String getCmd() {
        return this.cmd;
    }

    @Override
    public String toString() {
        return "CronJobDefinition{" +
                "cronSchedule=" + cronSchedule +
                ", cmd='" + cmd + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CronJobDefinition that = (CronJobDefinition) o;
        return Objects.equals(cronSchedule, that.cronSchedule) && Objects.equals(cmd, that.cmd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cronSchedule, cmd);
    }
}
