package org.example.cron.parser;

import java.util.List;
import java.util.stream.IntStream;

import org.example.cron.model.CronJobDefinition;
import org.example.cron.model.CronSchedule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CronExpressionParserTest {

    private static final CronExpressionParser cronExpressionParser = new CronExpressionParser();

    @Test
    public void test() {
        String cronJobDefinition = "*/15 0 1,15 * 1-5 /usr/bin/find";
        List<Integer> minutes = List.of(0,15,30,45);
        List<Integer> hours = List.of(0);
        List<Integer> dom = List.of(1,15);
        List<Integer> months = List.of(1,2,3,4,5,6,7,8,9,10,11,12);
        List<Integer> dow = List.of(1,2,3,4,5);
        CronSchedule cronSchedule = CronSchedule.builder()
                .withMinutes(minutes)
                .withHours(hours)
                .withMonths(months)
                .withDaysOfMonth(dom)
                .withDaysOfWeek(dow)
                .build();
        CronJobDefinition expectedJobDefinition = new CronJobDefinition(cronSchedule, "/usr/bin/find");
        Assertions.assertEquals(expectedJobDefinition, cronExpressionParser.parse(cronJobDefinition), "Basic test");

    }
}
