package org.example.cron.parser;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DayOfWeekFieldParserUnitTest {

    private static final DayOfWeekFieldParser parser = new DayOfWeekFieldParser();

    @Test
    public void testValidExpr() {
        Assertions.assertTrue(parser.validateExpression("*"), "Valid All");
        Assertions.assertTrue(parser.validateExpression("0"), "Valid single entry");
        Assertions.assertTrue(parser.validateExpression("0-59"), "Valid range");
        Assertions.assertTrue(parser.validateExpression("0,1,3,5"), "Valid multiple entry");
        Assertions.assertTrue(parser.validateExpression("3,5"), "Valid multiple entry");
        Assertions.assertFalse(parser.validateExpression("0-24/10"), "Valid range with increment");
        Assertions.assertFalse(parser.validateExpression("*/10"), "Valid entire range with increment");

        Assertions.assertFalse(parser.validateExpression("0-1-12"), "Valid range with increment");
        Assertions.assertFalse(parser.validateExpression("0-24-12/10"), "Valid range with increment");
        Assertions.assertFalse(parser.validateExpression(",5"), "Invalid multiple entry");
        Assertions.assertFalse(parser.validateExpression("5,"), "Invalid multiple entry");
        Assertions.assertFalse(parser.validateExpression("-1"), "Invalid negative");
        Assertions.assertFalse(parser.validateExpression("0,1,3/10"), "Invalid specific list and increment together");
    }

    @Test
    public void testParse() {
        assertEquals(IntStream.rangeClosed(0, 6).toArray(), parser.parse("*"), "Valid All");
        assertEquals(new int[]{0}, parser.parse("0"), "Valid single entry");
        assertEquals(IntStream.rangeClosed(0, 5).toArray(), parser.parse("0-5"), "Valid range");
        assertEquals(new int[]{0, 1, 3, 5}, parser.parse("0,1,3,5"), "Valid multiple entry");
        assertEquals(new int[]{0, 1, 3, 5}, parser.parse("0,1,5,3"), "Valid multiple entry not sorted");
        assertEquals(new int[]{3, 5}, parser.parse("3,5"), "Valid multiple entry two elements");

        assertFailInvalidExpr("0-1-12");
        assertFailInvalidExpr("0,1,3/10");
        assertFailInvalidExpr("*/15");
        assertFailValuesNotInRange("15");

    }

    private void assertFailInvalidExpr(String expr) {
        try {
            parser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Invalid cron expression : " + expr, e.getMessage());
        }
    }
    private void assertFailInvalidRange(String expr) {
        try {
            parser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Invalid range", e.getMessage());
        }
    }

    private void assertFailValuesNotInRange(String expr) {
        try {
            parser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Values not in range", e.getMessage());
        }
    }

    private void assertEquals(int[] expected, List<Integer> actual, String msg) {
        Assertions.assertArrayEquals(expected, actual.stream().mapToInt(Integer::intValue).toArray(), msg);
    }
}
