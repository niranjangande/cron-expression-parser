package org.example.cron.parser;

import java.util.List;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class NumericFieldRangeParserUnitTest {

    private static final NumericFieldRangeParser rangeParser = new NumericFieldRangeParser(0, 59);

    @Test
    public void testValidExpr() {
        Assertions.assertTrue(rangeParser.validateExpression("*"), "Valid All");
        Assertions.assertTrue(rangeParser.validateExpression("0"), "Valid single entry");
        Assertions.assertTrue(rangeParser.validateExpression("0-59"), "Valid range");
        Assertions.assertTrue(rangeParser.validateExpression("0,1,3,5"), "Valid multiple entry");
        Assertions.assertTrue(rangeParser.validateExpression("3,5"), "Valid multiple entry");
        Assertions.assertTrue(rangeParser.validateExpression("0-24/10"), "Valid range with increment");
        Assertions.assertTrue(rangeParser.validateExpression("*/10"), "Valid entire range with increment");

        Assertions.assertFalse(rangeParser.validateExpression("0-1-12"), "Valid range with increment");
        Assertions.assertFalse(rangeParser.validateExpression("0-24-12/10"), "Valid range with increment");
        Assertions.assertFalse(rangeParser.validateExpression(",5"), "Invalid multiple entry");
        Assertions.assertFalse(rangeParser.validateExpression("5,"), "Invalid multiple entry");
        Assertions.assertFalse(rangeParser.validateExpression("-1"), "Invalid negative");
        Assertions.assertFalse(rangeParser.validateExpression("0,1,3/10"), "Invalid specific list and increment together");
    }

    @Test
    public void testParse() {
        assertEquals(IntStream.rangeClosed(0, 59).toArray(), rangeParser.parse("*"), "Valid All");
        assertEquals(new int[]{0}, rangeParser.parse("0"), "Valid single entry");
        assertEquals(IntStream.rangeClosed(0, 59).toArray(), rangeParser.parse("0-59"), "Valid range");
        assertEquals(new int[]{0, 1, 3, 5}, rangeParser.parse("0,1,3,5"), "Valid multiple entry");
        assertEquals(new int[]{0, 1, 3, 5}, rangeParser.parse("0,1,5,3"), "Valid multiple entry not sorted");
        assertEquals(new int[]{3, 5}, rangeParser.parse("3,5"), "Valid multiple entry two elements");
        assertEquals(new int[]{0, 10, 20}, rangeParser.parse("0-29/10"), "Valid range with increment");
        assertEquals(new int[]{0, 10, 20, 30, 40}, rangeParser.parse("0-40/10"), "Valid range with increment inclusive");
        assertEquals(new int[]{0, 15, 30, 45}, rangeParser.parse("*/15"), "Valid entire range with increment");

        assertFailInvalidExpr("0-1-12");
        assertFailInvalidExpr("0,1,3/10");
        assertFailInvalidRange("0-60");
        assertFailValuesNotInRange("1,2,60");
        assertFailValuesNotInRange("60");
    }

    private void assertFailInvalidExpr(String expr) {
        try {
            rangeParser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Invalid cron expression : " + expr, e.getMessage());
        }
    }

    private void assertFailInvalidRange(String expr) {
        try {
            rangeParser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Invalid range", e.getMessage());
        }
    }

    private void assertFailValuesNotInRange(String expr) {
        try {
            rangeParser.parse(expr);
            Assertions.fail("Expected exception");
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals("Values not in range", e.getMessage());
        }
    }

    private void assertEquals(int[] expected, List<Integer> actual, String msg) {
        Assertions.assertArrayEquals(expected, actual.stream().mapToInt(Integer::intValue).toArray(), msg);
    }
}
