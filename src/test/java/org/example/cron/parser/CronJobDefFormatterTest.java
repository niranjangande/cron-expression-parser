package org.example.cron.parser;

import java.util.List;

import org.example.cron.format.CronJobDefFormatter;
import org.example.cron.model.CronJobDefinition;
import org.example.cron.model.CronSchedule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CronJobDefFormatterTest {

    private static final CronJobDefFormatter cronJobDefFormatter = new CronJobDefFormatter();

    @Test
    public void test() {
        List<Integer> minutes = List.of(0,15,30,45);
        List<Integer> hours = List.of(0);
        List<Integer> dom = List.of(1,15);
        List<Integer> months = List.of(1,2,3,4,5,6,7,8,9,10,11,12);
        List<Integer> dow = List.of(1,2,3,4,5);
        CronSchedule cronSchedule = CronSchedule.builder()
                .withMinutes(minutes)
                .withHours(hours)
                .withMonths(months)
                .withDaysOfMonth(dom)
                .withDaysOfWeek(dow)
                .build();
        CronJobDefinition cronJobDefinition = new CronJobDefinition(cronSchedule, "/usr/bin/find");

        String expected =
                """
                        minute        0 15 30 45
                        hour          0
                        day of month  1 15
                        month         1 2 3 4 5 6 7 8 9 10 11 12
                        day of week   1 2 3 4 5
                        command       /usr/bin/find""";

        Assertions.assertEquals(expected, cronJobDefFormatter.prettyPrint(cronJobDefinition), "Basic test");
    }
}
